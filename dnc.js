const express = require('express'),
    http = require('http'),
    app = express(),
    sio = require('socket.io'),
    morgan = require('morgan'),
    routes = require('./routes'),
    session = require('express-session'),
    RedisStore = require('connect-redis')(session),
    bodyParser = require('body-parser'),
    path = require('path'),
    stylus = require('stylus'),
    autoprefixer = require('autoprefixer-stylus'),
    csso = require('csso-stylus'),
    server = http.createServer(app),
    socket = require('./socket/socket.js')

io = sio.listen(server);
socket.socket(io)
app.disable('x-powered-by')


app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(morgan('dev'))
app.use(session({
    name: 'Lingia[ART]',
    store: new RedisStore({
        // host: '127.0.0.1',
        db: 1
    }),
    saveUninitialized:false,
    secret: 'xfgncfgncfgn'
}));

app.use(stylus.middleware({
    src: __dirname + '/public/stylesheets/src/',
    dest: __dirname + '/public/stylesheets',
    debug: true,
    compile: function(str, path) {
        return stylus(str)
            .use(autoprefixer())
            .use(csso())
            .set('filename', path)
            .set('compress', true)
    }
}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(routes)
app.use(require('./routes/Lead'))
app.use(require('./routes/User'))

server.listen(8124, function() {
    console.log('server running at port 8124');
});
