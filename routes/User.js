'use strict'
const router=require('express').Router(),
	user=require('../models/user')

router.get('/log-in',function(req,res){
	res.render('login.pug',{page_name:"Authorization"})
})
router.get('/new-user',function(req,res){
	res.render('new_user.pug',{page_name:"New user"})
})

router.post('/new-user',user.newUser)

module.exports=router