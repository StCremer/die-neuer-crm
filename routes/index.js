'use strict'
const router=require('express').Router()

router.get('/clients',function(req,res){
	res.render('clients.pug',{page_name:"Клиенты",newItemTitle:"+ новый клиент",newItemUrl:'/client'})
	// res.redirect('/events')
})
router.get('/',function(req,res){
	res.render('leads.pug',{page_name:"Заявки", newItemTitle:"+ новая заявка",newItemUrl:'/new-lead'})
	// res.redirect('/events')
})
router.get('/new-lead',function(req,res){
	res.render('lead.pug',{page_name:"Новая заявка"})
	// res.redirect('/events')
})
router.get('/client',function(req,res){
	res.render('client.pug',{page_name:"Клиент"})
	// res.redirect('/events')
})

module.exports=router