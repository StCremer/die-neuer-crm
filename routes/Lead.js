'use strict'
const router=require('express').Router(),
	lead=require('../models/lead')

router.get('/new-lead',function(req,res){
	res.render('lead.pug',{page_name:"Новая заявка"})
	// res.redirect('/events')
})

router.post('/new-lead',lead.newLead)

module.exports=router