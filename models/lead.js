'use strict'
const socket = require('../socket/socket.js').connection,
    mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId
    // console.log('socket->',socket,'<-socket')

mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost/DNC');
const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log("we're connected!")
});

let leadSchema = mongoose.Schema({
    user: {
        name: String,
        surname: String,
        email: String
    }, //id
    product: Array,
    creationDate: { type: Date },
    status: ObjectId, //status id
    utm: String,
    ip: String,
    lastWatchers: Array,
    ownerGroup: String,
    address: {
        cityid: Number, // продумать хранение адреса
        countryid: Number
    },
    confirmationDate: { type: Date },
    delivery: { company: String },

}, { autoIndex: false })

const Lead = mongoose.model('Lead', leadSchema)


module.exports.newLead = (req, res) => {
    console.log('req.body----->', req.body)

    let lead = new Lead({
        user: {
            name: req.body['user-name'],
            surname: req.body['user-surname'],
            fathername: req.body['user-father-name'],
            email: req.body['e-mail']
        },
        product: req.body['product-title'],
        creationDate: new Date
    })
    lead.save((err) => {
        if (err) return console.log(err)
    })
    socket.emit('added-new-lead', {
        userName: req.body['user-name'],
        userSurname: req.body['user-surname'],
        phone: req.body.Phone,
        product: req.body['product-title']

    })
    res.status(200).end()
}
