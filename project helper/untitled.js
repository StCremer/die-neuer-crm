{
    _id: ObjectId("5784c1cd6d0b2d8d4f743093"),
    offer: "string",
    name: {
        name: "string",
        surname: "string",
        nof: "string",
        fullname: this.name + this.surname + this.nof
    },
    site: "string",
    utm: {
        utm_campaign: "string",
        utm_content: "string",
        utm_source: "string",
        ..................
    },
    ip: "string",
    owner: "string",
    status: "string",
    address: "cityId",
    creationTime: "number",
    employee: [{
        name: "string",
        id: ObjectId("5783d3e09e286ba0181090f5"),
    }],
    confirmationTime:"date",
    delivery:"number",

}
