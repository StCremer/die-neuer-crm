'use strict'

function sendRequest(event) {
    event.preventDefault()

    let form = event.target.form,
        reqBody = {},
        fields = form.getElementsByTagName('input')

    for (let i = 0; i < fields.length; i++) {
        reqBody[fields[i].name] = fields[i].value
        console.log('element->', fields[i].name, '<---->', fields[i].value)
        if (i == fields.length - 1) {
            console.log("1 reqBody--->", reqBody)
            fetch('/new-lead', {
                    method: "POST",
                    headers:{"Content-Type":"application/json"},
                    body: JSON.stringify(reqBody)
                })
                .then(function(result) {
                    console.log('result->', result)
                })


        }
    }
}
